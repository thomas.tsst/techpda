# Documentation technique

### Introduction
Dans le cadre du cours "PDA", il nous a été demandé de réaliser un projet dans le domaine de notre futur travail de Diplôme, avec comme seul contrainte, l'utilisation d'un framework. Pour ma part, j'ai décidé de réaliser une application en React qui interagit avec une blockchain.

#### But du projet
Pour être capable d'interagir avec la blockchain, j'ai décidé d'utiliser la librairie Ethers.js. Pour avoir un controle total de la blockchain,j'utilise un serveur local [Hardhat](https://hardhat.org/) qui me premet de deployer des [contrats intelligents](https://fr.wikipedia.org/wiki/Contrat_intelligent) sur une copie de blockchain Ethereum.

#### Contraintes techniques
* Utiliser un framework

### Analyse

#### Technologies
* ReactJs
* Solidity (Contrat intelligent)
* Markdown
* GitLab (versionning)
* Visual Studio Code
* Mkdocs (Documentation)

#### Librairies et outils externes
##### React
![Image title](./img/react.png){ width="300"; }

React est une bibliothèque JavaScript qui a été développée par Facebook. Cette librairie a pour but de faciliter la création d'appplication web. Elle intègre la notion de composants, ces derniers, ont leurs propre état et permettent de générer des portions d'[HTML](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language) qui peuvent être disposées dans une même page.

##### Hardhat
![Image title](./img/hardhat.jpg){ width="300"; }

Hardhat vient considérablement simplifier le développement sur Ethereum

Il permet de :

* Disposer d’un nœud local Ethereum (hardhat network) avec des adresses déjà créées pour le développement (comme le fait Ganache). Il y a également la possibilité de créer une copie complète de la blockchain en production, ce qui permet le développement au plus proche de la réalité.
* Tester et débugger garce au terminal de commande qui fait tourner le serveur.
* Déployer des contrats intelligents
* Exposer le noeud de notre blockchain avec une interface JSON-RPC, ce qui permet à l'API [Metamask](https://metamask.io/) ou d'autres de se connecter au réseau.

##### Ethers.js
![Image title](./img/ethers.png){ width="300"; }

Ethers.js est une bibliothèque qui fournit un ensemble de fonctions qui permettent d'intéragir avec les noeuds Ethereum. Cette librairie, est très similaire au package Web3-eth les interactions possibles sont pareils.

##### Solidity
![Image title](./img/solidity.png ){ width="300"; }

Solidity est un language de programmation orientée objet créé pour le développement de contrat intelligent. il permet le déploiement sur plusieurs blockchain differentes dont celle d'ethereum. Il a été developpé par plusieurs ancien contributeur principaux d'ethereum.
#### Environnement de développement
* Windows 10

## Analyse fonctionnelle

### Interface
![Image title](./img/interface.png){ width="300"; }

L'interface de mon application est très basique. Les données jugé les plus importantes recupérées grâce au wallet avec lequel l'utilisateur c'est connecté sont disposé sur l'application.

En : 

**Rouge** : Dans ce champ l'utilisateur doit rentrer le montant en Ethereum qu'il veut déposer sur le contrat intélligent. 

**Vert** : Dans ce champ l'utilisateur doit rentrer le montant en Ethereum qu'il veut retirer sur le contrat intélligent. 

**Jaune** : Cette zone permet à l'utilisateur de retirer des Ethereum du contrat intelligent en direction de l'adresse de sont choix.

**Bleu** : Cette partie est simplement un receuille d'information que j'ai réussi a récuperer en parcourant la blockchain. Premièrement, il y a l'adresse public du wallet connecté dans mon application. Deuxièmement, il y a le nombre de transactions qu'il a effectué depuis la création du porte-monnaie. Enfin, il y a un listing des transactions avec comme information la date, les 2 adresse des wallet ayant effectuer cette transaction et le montant envoyé dans cette dernière.

### Fonctionnalités
### Transaction
L'application a été conçue pour permettre à l'utilisateur d'envoyer des Ethereum entre son wallet personnel et le contrat intelligent. Celui-ci, agit en tant que "porte-monnaie épragne", c'est pour cela qu'il à également la possibilité de retirer ses Ethereum personnel sur le contrat. Deux possibilités s'offrent à lui, soit il peut retirer les Ethereum sur le wallet avec lequel il a précédemment dépsoé, soit préciser une adresse différente sur laquelle il voudrait envoyé les Ethereum. 

### Historique des transactions
L'application rend possible à l'utilisateur de visualiser les transactions qu'il a effectués avec le wallet connecté dans l'application. Une transaction s'affiche avec la date, l'adresse de l'expéditeur, l'addresse du destinataire et le montant de la transaction.
## Analyse organique

#### documentationTechnique.md 
Fichier Markdown contenant toute la documentation technique. Un pdf est généré à partir de ce fichier avec l'aide de Mkdocs-with-pdf

#### JournalDeBord.md
Fichier Markdown contenant un résumé de mon activité journalière.

#### App.js
Fichier JavaScript contenant la vue de mon application.

#### transactions.js
Fichier JavaScript contenant un composant React dans lequel s'y trouve toutes les fonctions nécessaires à l'affichage de l'historique des transactions.

#### deploy.js
Fichier JavaScript permettant le déploiement du contrat intelligent dans la blockchain

#### Wallet.sol
Fichier Solidity qui contient le contrat intelligent utilisé dans l'application.

### Architecture

#### Arborescence de fichier
```
+-- Doc
|	+-- documentationTechnique.md
|	+-- JournalDeBord.md
|   +-- CahierDesCharges.md
|   +-- planning.xlsx
|   +-- img       
+-- wallet
|   +-- contracts
|       +-- Wallet.sol
|   +-- node_module
|       +-- ethers
|   +-- scripts
|       +-- deploy.js
|   +-- src
|       +-- artifacts
|       +-- pages
|           +-- transactions.js
|       +-- App.js
```

#### documentationTechnique.md
Ce fichier contient la documentation de se projet sous format markdown. Pourra ensuite être convertis en pdf grâce a mkdocs.

#### JournalDeBord.md
Fichier markdown contenant journal de bord qui m'a suivi durant ce projet.

#### CahierDesCharges.md
Fichier markdown contenant le cahier des charges du projet.

#### planning.xlxs
Fichier excel contenant le planning prévisionnel de mon projet.

#### Wallet.sol
Ficheir solidity contenant le smart contract utilisé dans mon application.

#### deploy.js
Script javascript permetant de deployer le smart contract dans la blockchain.

#### transactions.js
Fichier javascript contenant un composant react qui me permet d'afficher les transaction que le wallet a effectué.

#### App.js
Fichier javascript contenant le page principal de mon application.

## Conclusion
### Amélioration possible
Dans le but d'améliorer ce projet il serait intérressant de mettre en place, comme indiqué dans mon cahier des charges, une fonctionnalité qui permette de créer et d'importer un wallet. Cela nous offre la possibilité de se séparer d'application tierce comme MetaMask. Ensuite ce qui serait également attrayant, ça serait d'améliorer l'interface utilisateur afin que cela soit le plus érgonomique possible.

### Bilan personnel
J'ai trouvé que malgré les difficultés rencontrées lors de l'élaboration de cette applications, que cela m'était très intérressant d'apprendre à développer des fonctions en relation direct avec la blockchain. Malgré tout, il m'est important de souligner que l'utilisation de React demeure très compliqué et très difficle de s'intruire par internet, puisque régulièrement je trouvais des avis divergents sur la conception d'une application en React. En rechanche, la création de contrat intelligent en Solidity m'a paru plus intuitif et simple d'utilisation, puisque les structures de données, par exemple les tableaux indexés mises à disposition facilitent le stockage de données.
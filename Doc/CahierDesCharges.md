# Cahier des charges

## Sujet
Création d'un gestionnaire de portefeuilles crypto.

## But
Créer une app VueJS poposant la création de différents porte feuilles crypto. Et de rendre possible l'échange entre les différentes cryptomonnaies. Rendre plus simple le suivis pour les personnes détenant plusieurs portefeuilles. Visualiser simplement l'évolution des portefeuilles.

## Spécifications
Au démarage de l'application il est demandé de se connnecter ou créer un compte (Double authentification Google). Quand l'utilisateur sera connecté il lui sera possible de créer ou d'importer un portefeuille (HD Wallet) de cryptomonnaie ERC-20 (Blockchain Ethereum). L'utilisateur aura également la possibilité d'échanger sa cryptomonnaie contre celle de son choix disponible dans la liste des cryptomonnaies supportées par l'application. Il y aura une Form qui permet de visualiser la valeur total ou celle des différents portefeuilles. Un interface permetant de suivre les profits ou les pertes sur un lapse de temps que l'utilisateur peut modifier. 
## Restriction
Aucune
## Environnement
### Languague de programmation

###  Système d'exploitation
- Windows 10
### Versionning
- GitHub
## Livrable
- Fichier zip contenant le projet
- Documentation et poster au format PDF
# Planning
Fichier .xlsx dispo a la racine de ce git.
